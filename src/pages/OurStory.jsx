import React from "react";
import { Link } from "react-router-dom";

import { Image } from "react-bootstrap"; 

import { BsCheck2All, BsCheck } from "react-icons/bs";
import { BsTwitter, BsFacebook, BsInstagram, BsLinkedin } from "react-icons/bs";

import SrctionPageHeader from '../sections/SrctionPageHeader';
import SrctionPageFooter from '../sections/SrctionPageFooter';
import SectionBreadcrumbs from '../sections/SrctionBreadcrumbs';

import introImg from "../assets/img/intro.jpg";

import members1 from "../assets/img/members/featured-members-1.jpg";
import members2 from "../assets/img/members/featured-members-2.jpg";
import members3 from "../assets/img/members/featured-members-3.jpg";
import members4 from "../assets/img/members/featured-members-4.jpg";

import membersUse1 from "../assets/img/members/member-1.jpg";
import membersUse2 from "../assets/img/members/member-2.jpg";
import membersUse3 from "../assets/img/members/member-3.jpg";


// OurStory.jsx
const OurStory = () => {
  return (
    <>
         <SrctionPageHeader />

      <main>
       
      <SectionBreadcrumbs pageNameTitle='Our Story' />

      
    <section id="story-intro" className="story-intro">
      <div className="container">

        <div className="row">
          <div className="col-lg-6 order-1 order-lg-2">

          <Image src={introImg} className="img-fluid" alt="My IMG" />
            
          </div>
          <div className="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content">
            <h3>Voluptatem dignissimos provident quasi corporis voluptates sit assumenda.</h3>
            <p className="fst-italic">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
              magna aliqua.
            </p>
            <ul>
              <li><i className="bi bi-check-circled"> <BsCheck2All /> </i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
              <li><i className="bi bi-check-circled"> <BsCheck2All /> </i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
              <li><i className="bi bi-check-circled"> <BsCheck2All /> </i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.</li>
            </ul>
            <p>
              Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
              velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
              culpa qui officia deserunt mollit anim id est laborum
            </p>
          </div>
        </div>

      </div>
    </section> 


    <section id="featured-members" className="featured-members">
      <div className="container">

        <div className="row content">
          <div className="col-lg-6">
          <Image src={members1} className="img-fluid" alt="My IMG" />  
    
          </div>
          <div className="col-lg-6 pt-3 pt-lg-0">
            <h3>Voluptatem dignissimos provident quasi corporis voluptates sit assumenda.</h3>
            <p className="fst-italic">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
              magna aliqua.
            </p>
            <ul>
              <li><i className="bi bi-check"> <BsCheck /> </i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
              <li><i className="bi bi-check"> <BsCheck /> </i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
            </ul>
          </div>
        </div>

        <div className="row content">
          <div className="col-lg-6 order-1 order-lg-2">
            <Image src={members2} className="img-fluid" alt="My IMG" />  
            
          </div>
          <div className="col-lg-6 order-2 order-lg-1 pt-3 pt-lg-0">
            <h3>Corporis temporibus maiores provident</h3>
            <p className="fst-italic">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
              magna aliqua.
            </p>
            <p>
              Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
              velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
              culpa qui officia deserunt mollit anim id est laborum
            </p>
          </div>
        </div>

        <div className="row content">
          <div className="col-lg-6">
             <Image src={members3} className="img-fluid" alt="My IMG" />  
            
          </div>
          <div className="col-lg-6 pt-3 pt-lg-0">
            <h3>Sunt consequatur ad ut est nulla consectetur reiciendis animi voluptas</h3>
            <p>Cupiditate placeat cupiditate placeat est ipsam culpa. Delectus quia minima quod. Sunt saepe odit aut quia voluptatem hic voluptas dolor doloremque.</p>
            <ul>
              <li><i className="bi bi-check"> <BsCheck /> </i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
              <li><i className="bi bi-check"> <BsCheck /> </i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
              <li><i className="bi bi-check"> <BsCheck /> </i> Facilis ut et voluptatem aperiam. Autem soluta ad fugiat.</li>
            </ul>
          </div>
        </div>

        <div className="row content">
          <div className="col-lg-6 order-1 order-lg-2">
           <Image src={members4} className="img-fluid" alt="My IMG" />  
            
          </div>
          <div className="col-lg-6 order-2 order-lg-1 pt-3 pt-lg-0">
            <h3>Quas et necessitatibus eaque impedit ipsum animi consequatur incidunt in</h3>
            <p className="fst-italic">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
              magna aliqua.
            </p>
            <p>
              Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
              velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
              culpa qui officia deserunt mollit anim id est laborum
            </p>
          </div>
        </div>
      </div>
    </section>


    <section id="members" className="members">
      <div className="container">

        <div className="row">

          <div className="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div className="member">
              <div className="member-img">
              <Image src={membersUse1} className="img-fluid" alt="My IMG" />  
                
                <div className="social">
                  <a href=""><i className="bi bi-twitter"> <BsTwitter />  </i></a>
                  <a href=""><i className="bi bi-facebook"> <BsFacebook />  </i></a>
                  <a href=""><i className="bi bi-instagram"> <BsInstagram />  </i></a>
                  <a href=""><i className="bi bi-linkedin">  <BsLinkedin />  </i></a>
                </div>
              </div>
              <div className="member-info">
                <h4>Amanda Jepson</h4>
                <span>Accountant</span>
                <p>Sint qui cupiditate. Asperiores fugit impedit aspernatur et mollitia. Molestiae qui placeat labore assumenda id qui nesciunt quo reprehenderit. Rem dolores similique quis soluta culpa enim quia ratione ea.</p>
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div className="member">
              <div className="member-img">
              <Image src={membersUse2} className="img-fluid" alt="My IMG" />  
                <div className="social">
                  <a href=""><i className="bi bi-twitter"> <BsTwitter />  </i></a>
                  <a href=""><i className="bi bi-facebook"> <BsFacebook />  </i></a>
                  <a href=""><i className="bi bi-instagram"> <BsInstagram />  </i></a>
                  <a href=""><i className="bi bi-linkedin">  <BsLinkedin />  </i></a>
                </div>
              </div>
              <div className="member-info">
                <h4>Niall Katz</h4>
                <span>Marketing</span>
                <p>Aut ex esse explicabo quia harum ea accusamus excepturi. Temporibus at quia quisquam veritatis impedit. Porro laborum voluptatum sed necessitatibus a saepe. Deserunt laborum quasi consequatur voluptatum iusto sint qui fuga vel. Enim eveniet sed quibusdam rerum in. Non dicta architecto consequatur quo praesentium nesciunt.</p>
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div className="member">
              <div className="member-img">
              <Image src={membersUse3} className="img-fluid" alt="My IMG" />  
                <div className="social">
                  <a href=""><i className="bi bi-twitter"> <BsTwitter />  </i></a>
                  <a href=""><i className="bi bi-facebook"> <BsFacebook />  </i></a>
                  <a href=""><i className="bi bi-instagram"> <BsInstagram />  </i></a>
                  <a href=""><i className="bi bi-linkedin">  <BsLinkedin />  </i></a>
                </div>
              </div>
              <div className="member-info">
                <h4>Demi Lewis</h4>
                <span>Financing</span>
                <p>Amet labore numquam corrupti est. Nostrum amet voluptas consectetur dolor voluptatem architecto distinctio consequuntur eligendi. Quam impedit enim aut nesciunt aut dicta quam exercitationem. Reprehenderit exercitationem magnam. Ullam similique ut voluptas totam nobis porro accusamus nulla omnis.</p>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section>
        
      </main>

      <SrctionPageFooter />
      
    </>
  );
};

export default OurStory;
