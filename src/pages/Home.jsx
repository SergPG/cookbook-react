import React from "react";
// import { Link } from "react-router-dom";

import "./pages.scss";

import SrctionPageHeader from "../sections/SrctionPageHeader";

import SrctionHero from "../sections/SrctionHero";

import SectionAbout from "../sections/SrctionAbout";

import SrctionFeatures from "../sections/SrctionFeatures";

import SrctionRecentPhotos from  "../sections/SrctionRecentPhotos";

import SrctionPageFooter from "../sections/SrctionPageFooter";


// Home.jsx
const Home = () => {
  return (
    <>
      
       <SrctionPageHeader />

       <SrctionHero />
       
      <main id="main">

               
           <SectionAbout />
           <SrctionFeatures />
           <SrctionRecentPhotos />
        
      </main>

      < SrctionPageFooter />

      
    </>
  );
};

export default Home;
