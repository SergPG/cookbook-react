import React from "react";

import { Image } from "react-bootstrap"; 

import SrctionPageHeader from '../sections/SrctionPageHeader';
import SrctionPageFooter from '../sections/SrctionPageFooter';
import SectionBreadcrumbs from '../sections/SrctionBreadcrumbs';

import home_1 from "../assets/img/gallery/home-1.jpg";
import home_2 from "../assets/img/gallery/home-2.jpg";
import home_3 from "../assets/img/gallery/home-3.jpg";

import beach_1 from "../assets/img/gallery/beach-1.jpg";
import beach_2 from "../assets/img/gallery/beach-2.jpg";
import beach_3 from "../assets/img/gallery/beach-3.jpg";

import vacation_1 from "../assets/img/gallery/vacation-1.jpg";
import vacation_2 from "../assets/img/gallery/vacation-2.jpg";
import vacation_3 from "../assets/img/gallery/vacation-3.jpg";


// Gallery.jsx
const Gallery = () => {
  return (
    <>
         <SrctionPageHeader />

      <main>
       
       <SectionBreadcrumbs pageNameTitle='Gallery' />

       <section id="gallery" className="gallery">
      <div className="container">

        <div className="row">
          <div className="col-lg-12 d-flex justify-content-center">
            <ul id="gallery-flters">
              <li data-filter="*" className="filter-active">All</li>
              <li data-filter=".filter-home">Home</li>
              <li data-filter=".filter-beach">Beach</li>
              <li data-filter=".filter-vacation">Vacation</li>
            </ul>
          </div>
        </div>

        <div className="row gallery-container">

          <div className="col-lg-4 col-md-6 gallery-item filter-home">
            <div className="gallery-wrap">
               <Image src={home_1}  className="img-fluid" alt="MyIMG" />    
              
              <div className="gallery-info">
                <h4>Home 1</h4>
                <p>Home</p>
                <div className="gallery-links">
                  <a href={home_1} className="glightbox" title="Home 1"><i className="bx bx-plus"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-6 gallery-item filter-vacation">
            <div className="gallery-wrap">
                <Image src={vacation_2}  className="img-fluid" alt="MyIMG" />  

              <div className="gallery-info">
                <h4>Vacation 2</h4>
                <p>Vacation</p>
                <div className="gallery-links">
                  <a href="../assets/img/gallery/vacation-2.jpg" className="glightbox" title="Vacation 2"><i className="bx bx-plus"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-6 gallery-item filter-home">
            <div className="gallery-wrap">
                <Image src={home_2}  className="img-fluid" alt="MyIMG" /> 
    
              <div className="gallery-info">
                <h4>Home 2</h4>
                <p>Home</p>
                <div className="gallery-links">
                  <a href={home_2} className="glightbox" title="Home 2"><i className="bx bx-plus"></i></a>
                </div>
              </div>
            </div>
          </div>
    

          <div className="col-lg-4 col-md-6 gallery-item filter-home">
            <div className="gallery-wrap">

            <Image src={home_3}  className="img-fluid" alt="MyIMG" /> 

              <div className="gallery-info">
                <h4>Home 3</h4>
                <p>Home</p>
                <div className="gallery-links">
                  <a href={home_3} className="glightbox" title="Home 3"><i className="bx bx-plus"></i></a>
                </div>
              </div>
            </div>
          </div>




        

          <div className="col-lg-4 col-md-6 gallery-item filter-vacation">
            <div className="gallery-wrap">

            <Image src={vacation_1}  className="img-fluid" alt="MyIMG" />   
              
              <div className="gallery-info">
                <h4>Vacation 1</h4>
                <p>Vacation</p>
                <div className="gallery-links">
                  <a href={vacation_1} className="glightbox" title="Vacation 1"><i className="bx bx-plus"></i></a>
                </div>
              </div>
            </div>
          </div>
          
           <div className="col-lg-4 col-md-6 gallery-item filter-beach">
            <div className="gallery-wrap">
              <Image src={beach_2}  className="img-fluid" alt="MyIMG" /> 
             
            
      <div className="gallery-info">
                <h4>Beach 2</h4>
                <p>Beach</p>
                <div className="gallery-links">
                  <a href={beach_2} className="glightbox" title="Beach 2"><i className="bx bx-plus"></i></a>
                </div>
              </div>
            </div>
          </div>


          <div className="col-lg-4 col-md-6 gallery-item filter-beach">
            <div className="gallery-wrap">

            <Image src={beach_1}  className="img-fluid" alt="MyIMG" />   
              
              <div className="gallery-info">
                <h4>Beach 1</h4>
                <p>Beach</p>
                <div className="gallery-links">
                  <a href={beach_1} className="glightbox" title="Beach 1"><i className="bx bx-plus"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-6 gallery-item filter-beach">
            <div className="gallery-wrap">

            <Image src={beach_3}  className="img-fluid" alt="MyIMG" />  

             
              <div className="gallery-info">
                <h4>Beach 3</h4>
                <p>Beach</p>
                <div className="gallery-links">
                  <a href={beach_3}  className="glightbox" title="Beach 3"><i className="bx bx-plus"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-6 gallery-item filter-vacation">
            <div className="gallery-wrap">

            <Image src={vacation_3}  className="img-fluid" alt="MyIMG" /> 

              <div className="gallery-info">
                <h4>Vacation 3</h4>
                <p>Vacation</p>
                <div className="gallery-links">
                  <a href={vacation_3} className="glightbox" title="Vacation 3"><i className="bx bx-plus"></i></a>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section> 
       
      </main>

      <SrctionPageFooter />
      
    </>
  );
};

export default Gallery;
