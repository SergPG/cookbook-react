import React from "react";

import { Image } from "react-bootstrap";

import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Pagination } from 'swiper';

// Styles must use direct files imports
import 'swiper/scss';
import 'swiper/scss/navigation';
import 'swiper/scss/pagination'; // Pagination module

import Photos1 from "../assets/img/recent-photos/recent-photos-1.jpg";
import Photos2 from "../assets/img/recent-photos/recent-photos-2.jpg";
import Photos3 from "../assets/img/recent-photos/recent-photos-3.jpg";
import Photos4 from "../assets/img/recent-photos/recent-photos-4.jpg";
import Photos5 from "../assets/img/recent-photos/recent-photos-5.jpg";
import Photos6 from "../assets/img/recent-photos/recent-photos-6.jpg";
import Photos7 from "../assets/img/recent-photos/recent-photos-7.jpg";
import Photos8 from "../assets/img/recent-photos/recent-photos-8.jpg";

// About.jsx
const SrctionRecentPhotos = () => {
  return (
    <>
      <section id="recent-photos" className="recent-photos">
        <div className="container">
          <div className="section-title">
            <h2>Recent Photos</h2>
            <p>
              Magnam dolores commodi suscipit. Necessitatibus eius consequatur
              ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam
              quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea.
              Quia fugiat sit in iste officiis commodi quidem hic quas.
            </p>
          </div>


          <Swiper
            slidesPerView={5}
            spaceBetween={30}
            
            loop={true}
            
            autoplay={{
              delay: 5000,
              disableOnInteraction: false,
            }}
            pagination={{
                 clickable: true,
               }}
        modules={[Autoplay, Pagination]}
        className="recent-photos-slider swiper"
      >
        <SwiperSlide>
          <a  href= {Photos1}
              className="glightbox"  >
           <Image
            src= {Photos1}
            className="img-fluid"
             alt=""
            />
          </a>
          </SwiperSlide>
          <SwiperSlide>
          <a  href= {Photos2}
              className="glightbox"  >
           <Image
            src= {Photos2}
            className="img-fluid"
             alt=""
            />
          </a>
          </SwiperSlide>
          <SwiperSlide>
          <a  href= {Photos3}
              className="glightbox"  >
           <Image
            src= {Photos3}
            className="img-fluid"
             alt=""
            />
          </a>
          </SwiperSlide>
          <SwiperSlide>
          <a  href= {Photos4}
              className="glightbox"  >
           <Image
            src= {Photos4}
            className="img-fluid"
             alt=""
            />
          </a>
          </SwiperSlide>
          <SwiperSlide>
          <a  href= {Photos5}
              className="glightbox"  >
           <Image
            src= {Photos5}
            className="img-fluid"
             alt=""
            />
          </a>
          </SwiperSlide>
          <SwiperSlide>
          <a  href= {Photos6}
              className="glightbox"  >
           <Image
            src= {Photos6}
            className="img-fluid"
             alt=""
            />
          </a>
          </SwiperSlide>
          <SwiperSlide>
          <a  href= {Photos7}
              className="glightbox"  >
           <Image
            src= {Photos7}
            className="img-fluid"
             alt=""
            />
          </a>
          </SwiperSlide>
          <SwiperSlide>
          <a  href= {Photos8}
              className="glightbox"  >
           <Image
            src= {Photos8}
            className="img-fluid"
             alt=""
            />
          </a>
          </SwiperSlide>

        
      </Swiper>



          
        </div>
      </section>
    </>
  );
};

export default SrctionRecentPhotos;
