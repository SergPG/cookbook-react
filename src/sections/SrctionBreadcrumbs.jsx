import React from "react";

import { Link } from "react-router-dom";

// SectionBreadcrumbs.jsx
const SectionBreadcrumbs = ({ pageNameTitle }) => {
  return (
    <>
    <section id="breadcrumbs" className="breadcrumbs">
      <div className="container">

        <div className="d-flex justify-content-between align-items-center">
          <h2> {pageNameTitle} </h2>
          <ol>
            <li> <Link to="/"  >Home</Link> </li>
            <li> {pageNameTitle} </li>
          </ol>
        </div>
      </div>
    </section>
    </>
  );
};

export default SectionBreadcrumbs;
