import React from "react";

import { Link } from "react-router-dom";

import { Navbar, Container, Nav, NavDropdown } from "react-bootstrap";

// About.jsx
const SrctionPageHeader = () => {
  return (
    <>
      <header id="header" className="fixed-top">


  <Navbar collapseOnSelect expand="lg" >

  <Container>

  <Navbar.Brand href="/">
      <h1 className="logo">  Me &amp; Family  </h1>  
  </Navbar.Brand>

  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    
    
    


  </Navbar.Collapse>
  </Container>
</Navbar>   
       
   <div className="container d-flex align-items-center justify-content-between">
          <h1 className="logo">
            <a href="index.html">Me &amp; Family</a>
          </h1>

          <nav id="navbar" className="navbar">
            <ul>
              <li>
                 <Link to="/"  >Home</Link>
              </li>
              <li>
                <Link to="/our-story"  >Our Story</Link>
              </li>
              <li>
                <Link to="/events"  >Events</Link>
              </li>
              <li>
                <Link to="/gallery"  >Gallery</Link>
              </li>
              <li className="dropdown">
                <a href="#">
                  <span>Drop Down</span> <i className="bi bi-chevron-down"></i>
                </a>
                <ul>
                  <li>
                    <a href="#">Drop Down 1</a>
                  </li>
                  <li className="dropdown">
                    <a href="#">
                      <span>Deep Drop Down</span>{" "}
                      <i className="bi bi-chevron-right"></i>
                    </a>
                    <ul>
                      <li>
                        <a href="#">Deep Drop Down 1</a>
                      </li>
                      <li>
                        <a href="#">Deep Drop Down 2</a>
                      </li>
                      <li>
                        <a href="#">Deep Drop Down 3</a>
                      </li>
                      <li>
                        <a href="#">Deep Drop Down 4</a>
                      </li>
                      <li>
                        <a href="#">Deep Drop Down 5</a>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <a href="#">Drop Down 2</a>
                  </li>
                  <li>
                    <a href="#">Drop Down 3</a>
                  </li>
                  <li>
                    <a href="#">Drop Down 4</a>
                  </li>
                </ul>
              </li>
              <li>
                <a href="contact.html">Contact</a>
              </li>
            </ul>
            <i className="bi bi-list mobile-nav-toggle"></i>
          </nav>
        </div>
      </header>
    </>
  );
};

export default SrctionPageHeader;
