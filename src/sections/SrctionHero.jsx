import React from "react";

import { Carousel } from "react-bootstrap";

import slideImg1 from "../assets/img/slide/slide-1.jpg";
import slideImg2 from "../assets/img/slide/slide-2.jpg";
import slideImg3 from "../assets/img/slide/slide-3.jpg";

// About.jsx
const SrctionHero = () => {
  return (
    <>
      <section id="hero">
        <Carousel id="heroCarousel" className="carousel slide carousel-fade">
          <Carousel.Item style={{ backgroundImage: `url(${slideImg1})` }}>
            {" "}
          </Carousel.Item>
          <Carousel.Item style={{ backgroundImage: `url(${slideImg2})` }}>
            {" "}
          </Carousel.Item>
          <Carousel.Item style={{ backgroundImage: `url(${slideImg3})` }}>
            {" "}
          </Carousel.Item>
        </Carousel>
      </section>
    </>
  );
};

export default SrctionHero;
