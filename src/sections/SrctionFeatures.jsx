import React from "react";

import { BsLaptop, BsBarChartLine, BsBoundingBox, BsBroadcast, BsCamera, BsDiagram3 } from "react-icons/bs";

// About.jsx
const SrctionFeatures = () => {
  return (
    <>
      <section id="features" className="features">
      <div className="container">

        <div className="row">
          <div className="col-lg-4 col-md-6 icon-box">
            <div className="icon"><i className="bi bi-laptop"><BsLaptop /></i></div>
            <h4 className="title"><a href="">Lorem Ipsum</a></h4>
            <p className="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
          </div>
          <div className="col-lg-4 col-md-6 icon-box">
            <div className="icon"><i className="bi bi-bar-chart"> <BsBarChartLine />   </i></div>
            <h4 className="title"><a href="">Dolor Sitema</a></h4>
            <p className="description">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p>
          </div>
          <div className="col-lg-4 col-md-6 icon-box">
            <div className="icon"><i className="bi bi-bounding-box"> <BsBoundingBox  />    </i></div>
            <h4 className="title"><a href="">Sed ut perspiciatis</a></h4>
            <p className="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p>
          </div>
          <div className="col-lg-4 col-md-6 icon-box">
            <div className="icon"><i className="bi bi-broadcast"> <BsBroadcast  />  </i></div>
            <h4 className="title"><a href="">Magni Dolores</a></h4>
            <p className="description">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
          </div>
          <div className="col-lg-4 col-md-6 icon-box">
            <div className="icon"><i className="bi bi-camera"> <BsCamera />   </i></div>
            <h4 className="title"><a href="">Nemo Enim</a></h4>
            <p className="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
          </div>
          <div className="col-lg-4 col-md-6 icon-box">
            <div className="icon"><i className="bi bi-diagram-3"> <BsDiagram3   /> </i></div>
            <h4 className="title"><a href="">Eiusmod Tempor</a></h4>
            <p className="description">Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi</p>
          </div>
        </div>

      </div>
    </section>
    </>
  );
};

export default SrctionFeatures;
