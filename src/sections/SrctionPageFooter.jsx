import React from "react";

import { BsTwitter, BsFacebook, BsInstagram, BsSkype, BsLinkedin } from "react-icons/bs";

// About.jsx
const SrctionPageFooter = () => {
  return (
    <>
      <footer id="footer">
        <div className="container">
          <h3>MeFamily</h3>
          <p>
            Et aut eum quis fuga eos sunt ipsa nihil. Labore corporis magni
            eligendi fuga maxime saepe commodi placeat.
          </p>
          <div className="social-links">
            <a href="#" className="twitter">
              <i className="bx bxl-twitter"> <BsTwitter />  </i>
            </a>
            <a href="#" className="facebook">
              <i className="bx bxl-facebook"> <BsFacebook />  </i>
            </a>
            <a href="#" className="instagram">
              <i className="bx bxl-instagram"><BsInstagram />  </i>
            </a>
            <a href="#" className="google-plus">
              <i className="bx bxl-skype"> <BsSkype /> </i>
            </a>
            <a href="#" className="linkedin">
              <i className="bx bxl-linkedin"> <BsLinkedin /> </i>
            </a>
          </div>
          <div className="copyright">
            &copy; Copyright{" "}
            <strong>
              <span>MeFamily</span>
            </strong>
            . All Rights Reserved
          </div>
          <div className="credits">
            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
          </div>
        </div>
      </footer>
    </>
  );
};

export default SrctionPageFooter;
