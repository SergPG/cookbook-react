import React from "react";
import { Routes, Route, Link } from "react-router-dom";
import logo from "./logo.svg";

import "./App.scss";

import  Home  from './pages/Home' ;
import  OurStory  from './pages/OurStory' ;
import Events from "./pages/Events";
import Gallery from "./pages/Gallery";




// App.js
// function Home() {
//   return (
//     <>
//       <main>
//         <h2>Welcome to the homepage!</h2>
//         <p>You can do this, I believe in you.</p>
//       </main>
//       <nav>
//         <Link to="/about">About</Link>
//       </nav>
//     </>
//   );
// }

// function About() {
//   return (
//     <>
//       <main>
//         <h2>Who are we?</h2>
//         <p>
//           That feels like an existential question, don't you
//           think?
//         </p>
//       </main>
//       <nav>
//         <Link to="/">Home</Link>
//       </nav>
//     </>
//   );
// }

function App() {
  return (
    <div className="App"  >
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="our-story" element={<OurStory />} />
        <Route path="events" element={<Events />} />
        <Route path="gallery" element={<Gallery />} />
      </Routes>
    </div>
  );
}

export default App;
